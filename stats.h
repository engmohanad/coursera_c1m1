/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief Header file for performing statistical analysis on a dataset.
 *
 * This file is the main header file that contains all the functions used
 * in the stats.c implementation file used to perform statistical analysis
 * on a dataset.
 *
 * @author Mohanad M.Marzouk
 * @date August 31, 2018
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/* Declaring Functions */ 

/**
 * @brief Finding the median number for a given array of data.
 * 
 * This function finds the median number of a given array of data, this is 
 * done by calling the function sort_array(), which is used to sort the array
 * in a descending order (largest to smallest), then finds the median number
 * of the sorted array.
 *
 * @param unsigned char array[], the given array of data.
 * @param int n, the number of elements of the given array of data.
 *
 * @return int median, the median number of the array.
 */
int find_median( unsigned char array[], int n );


/**
 * @brief Finding the mean value of a given array of data.
 *
 * This function is used to calculate the mean value of a given array of
 * data, this is done by summing all the array elements then dividing the result
 * by the number of elements.
 *
 * @param unsigned char array[], the given array of data.
 * @param int n, the number of elements of the given array.
 *
 * @return int mean, the mean value of the given array.
 */
int find_mean( unsigned char array[], int n );


/**
 * @brief Finding the maximum number of an array.
 *
 * This function is used to find the maximum number of a given array of data,
 * this is done by taking the first element of the array as an input and
 * compare this element with the remaining array elements, the element which
 * has a larger value than the input becomes the new input to be compared,
 * and so on until there is no element larger than the input element.
 *
 * @param unsigned char array[], the given array of data.
 * @param int n, the number of elements of the given array.
 *
 * @return unsigned char maximum, the maximum number of the given array.
 */
unsigned char find_maximum( unsigned char array[], int n );

/**
 * @brief Finding the minimum number of an array.
 *
 * This function is used to find the minimum number of a given array of data,
 * this is done by taking the first element of the array as an input and
 * compare this element with the remaining array elements, the element which
 * has a smaller value than the input becoms the new input to be compared,
 * and so on until there is no element smaller than the input element.
 *
 * @param unsigned char array[], the given array of data.
 * @param int n, the number of elements of the given array.
 *
 * @return unsigned char minimum, the minimum number of the given array.
 */
unsigned char find_minimum( unsigned char array[], int n );

/**
 * @brief Sorting a given array of data.
 *
 * This function is used to sort a given array in a descending order,
 * (largest to smallest), this is done by making an iteration to print the
 * elements of the array, then with a nested loop each element will be compared
 * to the remaining elements, the element which has a smaller value than the
 * input below it, the below element becomes interchanged, and so on until the
 * array is sorted.
 *
 * @param unsigned char *array, the value of array stored in the memory.
 * @param int n, the number of elements of the given array.
 *
 * @return array[], the array after sorting.
 */
void sort_array( unsigned char *array, int n );

/**
 * @brief Printing the given array of data.
 *
 * This function is used to print the given array of data and the number of
 * elements within this array on the screen, and in an easy way for the user
 * to read.
 *
 * @param unsigned char *array, the value of array stored in memory.
 * @param int n, the number of elements of the given array.
 *
 * @return the array will be printed on the screen.
 */
unsigned char print_array( unsigned char *array, int n );

/**
 * @brief Printing the calculated statistics
 *
 * This function is used to print the calculated statistics of a given array
 * of data on the screen, and in an easy way for the user to read, the
 * calculated statistics are: ( median number, maximum number, minimum number,
 * mean value).
 *
 * @param unsigned char *array, the value of array stored in memory.
 * @param int n, the number of elements of the given array.
 *
 * @return the statistics will be printed on the screen.
 */
unsigned char print_statistics( unsigned char *array, int n );



#endif /* __STATS_H__ */
