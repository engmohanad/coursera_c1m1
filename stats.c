/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief Perform statistical analysis on a dataset.
 *
 * This program is used to perform statistical analysis on a given array of
 * data then print the results on the command lind screen.
 *
 * @author Mohanad M.Marzouk
 * @date August 31, 2018
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

/* sorting the given array from largest to smallest */
void sort_array( unsigned char *array, int n){
	/* local varibales */
	int i = 0, j = 0, temp = 0;

	/* sorting iteration */
	for( i = 0 ; i < n ; i++ ){
		for( j = 0 ; j < n-1 ; j++ ){
			if( array[j] < array[j+1] ){
				temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
			}
		}
	}
	/* printing sorted array on the screen */
	printf( "\nThe Sorted Array of Data is:\n" );
	for( i = 0 ; i < n ; i++ ){
		printf( "%3d\t ", array[i] );
		if( ( i + 1 ) % 8 == 0 ){
			printf( "\n" );
		}
	}
	printf( "\n***************************\n" );
	
}

/* calculating median value */
int find_median( unsigned char array[], int n){
	/* local variables */
	int median = 0;

	/* for an array with even number of elements */
	if( n%2 == 0 )
		median = ( array[(n-1)/2] + array[n/2] ) / 2.0;
	/* for odd number of elements */
	else
		median = array[n/2];
	
	return median;
}

/* calculating mean value */
int find_mean( unsigned char *array, int n){
	/* local variables */
	int i;
	int mean = 0, sum = 0;

	/* summing all array elements */
	for( i = 0 ; i < n ; i++ ){
		sum += array[i];
	}
	mean = sum / n;     //calculating mean value

	return mean;
}

/* finding maximum number */
unsigned char find_maximum( unsigned char *array, int n){
	/* local variables */
	int i;
	unsigned char maximum = 0;

	maximum = array[0];     //assuming the first element to be the maximum value

	/* comparing elements iteration */
	for( i = 1 ; i < n ; i++ ){
		if( array[i] > maximum ){
			maximum = array[i];
		}
	}

	return maximum;
}

/* finding minimum number */
unsigned char find_minimum( unsigned char *array, int n){
	/* local variables */
	int i;
	unsigned char minimum = 0;

	minimum = array[0];    //assuming the first element to be the minimum value

	/* comparing elements iterarion */
	for( i = 1 ; i < n ; i++ ){
		if( array[i] < minimum ){
			minimum = array[i];
		}
	}

	return minimum;
}

/* printing the calculated statistics */
unsigned char print_statistics( unsigned char *array, int n){
	/* local variables */
	int i = 0;
	float median = 0, mean = 0;
	unsigned char maximum = 0, minimum = 0;

	//getting calculations from functions to the variables
	median = find_median( array, SIZE );
	mean = find_mean( array, SIZE );
	maximum = find_maximum( array, SIZE );
	minimum = find_minimum( array, SIZE );

	//printing results
	printf( "\nThe Calculated stats are as follows:\n" );
	printf( "***************\n" );
	printf( "\nThe Median Value is: %.0f\n", median );
	printf( "\nThe Mean Value is: %.0f\n", mean );
	printf( "\nThe Maximum Value is: %d\n", maximum );
	printf( "\nThe Minimum Value is: %d\n", minimum );
	printf( "\n************************************\n" );
}

/* printing the given array */
unsigned char print_array( unsigned char *array, int n){
	/* local variables */
	int i;

	//printing the given array
	printf( "\nThe Given Array of Data is:\n" );
	for( i = 0 ; i < n ; i++ ){
		printf( "%3d\t ", array[i] );
		if( ( i + 1 ) % 8 == 0 ){
			printf( "\n" );
		}
	}
	printf( "\n***************************\n" );
}

/* main function */
unsigned char main(){
	//given array of data
	unsigned char array[SIZE] = {  34, 201, 190, 154,   8, 194,   2,   6,
		                      114,  88,  45,  76, 123,  87,  25,  23,
				      200, 122, 150,  90,  92,  87, 177, 244,
				      201,   6,  12,  60,   8,   2,   5,  67,
				        7,  87, 250, 230,  99,   3, 100,  90};


	print_array( array, SIZE );       //printing the array directly
	sort_array( array, SIZE );        //sorting the array in a descending order
	print_statistics( array, SIZE );  //printing the calculated stats on the screen
}

