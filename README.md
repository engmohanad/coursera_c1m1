###****************************************************************************

### This is a free access repository for the Coursera Specialization of
### Embedded Software Essentials to be submitted with Course 1 Week 1 
### assignment and for peer grading purposes.

### The repository contains 3 files:
### stats.c -> this is the Implementation file for C-programming Code.
### stats.h -> this is the Header file for C-Programming Code.
### README.md -> this file contains the description of the repository.


